# Programming 1700 Fall 2022

This is a repository to support the learning for students at NSCC Lunenburg Campus in the Fall of 2022.  

The course is "Logic and Programming" and is based in Python.

The instructor is Brad Penney.
