#!/usr/bin/python3

import db_functions as dbf
import table_definitions as td

def main():

    # Create database if does not exist
    database = r'projectTaskTracker.db'

    # create a database connection
    conn = dbf.create_connection(database)
    
    # create the default tables to support projects and tasks
    dbf.create_table(conn, td.create_projects_table())
    dbf.create_table(conn, td.create_tasks_table())

    # main program logic begins here
    run_program = True
    print("##### Personal Project Tracker #####")
    while run_program:    
        print("""
        Please choose one of the following entries:
            1) View all projects
            2) View all tasks
            3) Create a new project
            4) Create a new task
            5) Delete a project
            6) Delete a task
            q) Quit the program
        """)

        userChoice = input("Select an option: ")

        with conn:
            # Quit the program
            if userChoice == 'q':
                run_program = False

            else:
                try:
                    isinstance(int(userChoice), int)
                    if int(userChoice) == 1:
                        print("\nThe projects stored in the database are: \n")
                        try:
                            print("ID#, Project, Start Date, End Date")
                            dbf.select_all_projects(conn)
                        except:
                            print("Sorry, there are no projects to display.\n")
                        input('\nPress "Enter" to continue ...\n')

                    # Show all tasks
                    elif int(userChoice) == 2:
                        print("\nThe tasks stored in the database are: \n")
                        print("ID#, Name, Priority, Status, Project Number, Start Date, End Date")
                        try:
                            dbf.select_all_tasks(conn)
                        except:
                            print("Sorry, there are no tasks to display.\n")
                        input('\nPress "Enter" to continue ...\n')
                    
                    # Add a project
                    elif int(userChoice) == 3:
                        project_name = input("What is the name of the project? ")
                        start_date = input("What is the start date? (Example - 2022-10-21) ")
                        end_date = input("What is the end date? (Example 2022-11-30) ")
                        project = (project_name, start_date, end_date)
                        dbf.create_project(conn, project)

                    # Add a task
                    elif int(userChoice) == 4:
                        try:
                            dbf.select_all_projects(conn)
                            project_id = input("What is the ID # of the associated project? ")
                            task_name = input("What is the name of the task? ")
                            start_date = input("What is the task start date? (Example - 2022-10-21) ")
                            end_date = input("What is the task end date? (Example 2022-11-30) ")
                            priority = input("What is the priority? (1 = High, 2 = Medium, 3 = Low) ")
                            status = input("What is the status? (1 = Waiting, 2 = In-Progress, 3 = Completed) ")
                            task = (task_name, priority, status, project_id, start_date, end_date)
                            dbf.create_task(conn, task)
                        except:
                            print("Sorry, your input was not recognized.  Hint: You must create a project before you can create a task.\n")

                    # Delete a Project
                    elif int(userChoice) == 5:
                        try:
                            print("ID#, Project, Start Date, End Date")
                            dbf.select_all_projects(conn)
                            del_project = int(input("Which project would you like to delete? "))
                            dbf.delete_project(conn, del_project)
                        except:
                            print("Sorry, there are no projects to display.\n")

                    # Delete a Task
                    elif int(userChoice) == 6:
                        try:
                            print("ID#, Name, Priority, Status, Project Number, Start Date, End Date")
                            dbf.select_all_tasks(conn)
                            del_task = int(input("Which task would you like to delete? "))
                            dbf.delete_task(conn, del_task)
                        except:
                            print("Sorry, there are no tasks to display.\n")

                # Catch all other inputs & loop back
                except:
                    print("Sorry, that isn't a recognzied command")

    conn.close() # Always close the database connection

if __name__ == '__main__':
    main()