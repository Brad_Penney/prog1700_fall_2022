#!/usr/bin/python3

##########################################
# Program: SpaceShip Battle              #
# Author: Tech with Tim                  #
# Source: https://youtu.be/jO6qQDNa2UY   #
# Modified By: Brad Penney               #
# Date: November 2022                    #
##########################################

#################
#### IMPORTS ####
#################
import pygame
import os
import datetime
pygame.font.init()
pygame.mixer.init()

##########################
#### GLOBAL VARIABLES ####
##########################
## GAME CONTROLS ##
FPS = 60 # Frames per second
VEL = 3 # Velocity of ships
BULLET_VEL = 7 # Velocity of bullets
MAX_BULLETS = 3 # Maximum number of bullets

## WINDOW PROPERTIES ##
WIDTH = 900
HEIGHT = 550
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Brad's First Game Ever")
SPACE = pygame.transform.scale(pygame.image.load(os.path.join('Assets', 'space.png')), (WIDTH, HEIGHT))
BORDER = pygame.Rect(WIDTH//2 - 5, 0, 10, HEIGHT)
HEALTH_FONT = pygame.font.SysFont('comicsans', 40)
WINNER_FONT = pygame.font.SysFont('comicsans', 100)

## COLOURS ##
WHITE = (255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
YELLOW = (255,255,0)

## SPACE SHIPS ##
SPACE_SHIP_WIDTH = 55
SPACE_SHIP_HEIGHT = 40
YELLOW_SPACE_SHIP = pygame.image.load(os.path.join('Assets', 'spaceship_yellow.png'))
YELLOW_SPACE_SHIP = pygame.transform.rotate(pygame.transform.scale(YELLOW_SPACE_SHIP, (SPACE_SHIP_WIDTH,SPACE_SHIP_HEIGHT)), 90)
RED_SPACE_SHIP = pygame.image.load(os.path.join('Assets', 'spaceship_red.png'))
RED_SPACE_SHIP = pygame.transform.rotate(pygame.transform.scale(RED_SPACE_SHIP, (SPACE_SHIP_WIDTH,SPACE_SHIP_HEIGHT)), 270)

## BULLETS ##
BULLET_HIT_SOUND = pygame.mixer.Sound(os.path.join('Assets','grenade.mp3'))
BULLET_FIRE_SOUND = pygame.mixer.Sound(os.path.join('Assets', 'gun_silencer.mp3'))
YELLOW_HIT = pygame.USEREVENT + 1
RED_HIT = pygame.USEREVENT + 2

################### 
#### FUNCTIONS ####
###################

def draw_window(red, yellow, red_bullets, yellow_bullets, red_health, yellow_health):
    """ Draws the window for the game
    :return: None
    """
    ## Main Window ##
    WIN.blit(SPACE, (0,0)) # Draw main window
    pygame.draw.rect(WIN, BLACK, BORDER) # Draw the border
    
    ## Scoring Text ##
    red_health_text = HEALTH_FONT.render(f"Health: {red_health}", 1, RED)
    yellow_health_text = HEALTH_FONT.render(f"Health: {yellow_health}", 1, YELLOW)
    WIN.blit(red_health_text, (WIDTH - red_health_text.get_width() - 10, 10))
    WIN.blit(yellow_health_text, (10, 10))

    ## Draw Spaceships ##
    WIN.blit(YELLOW_SPACE_SHIP, (yellow.x,yellow.y))
    WIN.blit(RED_SPACE_SHIP, (red.x, red.y))

    ## Draw Bullets ##
    for bullet in red_bullets:
        pygame.draw.rect(WIN,RED,bullet)
    for bullet in yellow_bullets:
        pygame.draw.rect(WIN,YELLOW,bullet)

    pygame.display.update() #Update the window

def yellow_handle_movement(keys_pressed, yellow):
    """ Controls movement for Yellow Ship
    :return None
    """
    if keys_pressed[pygame.K_a] and yellow.x - VEL > 0: # MOVE YELLOW LEFT
        yellow.x -= VEL
    if keys_pressed[pygame.K_d] and yellow.x + VEL < BORDER.x - SPACE_SHIP_HEIGHT: # MOVE YELLOW RIGHT
        yellow.x += VEL
    if keys_pressed[pygame.K_w] and yellow.y - VEL > 0: # MOVE YELLOW UP
        yellow.y -= VEL
    if keys_pressed[pygame.K_s] and yellow.y + VEL < HEIGHT - SPACE_SHIP_WIDTH: # MOVE YELLOW DOWN
        yellow.y += VEL

def red_handle_movement(keys_pressed, red):
    """ Control movement of red ship
    :return None
    """
    if keys_pressed[pygame.K_LEFT] and red.x - VEL > BORDER.x + BORDER.width: # MOVE RED LEFT
        red.x -= VEL
    if keys_pressed[pygame.K_RIGHT] and red.x + VEL < WIDTH - SPACE_SHIP_HEIGHT: # MOVE RED RIGHT
        red.x += VEL
    if keys_pressed[pygame.K_UP] and red.y - VEL > 0: # MOVE RED UP
        red.y -= VEL
    if keys_pressed[pygame.K_DOWN] and red.y + VEL < HEIGHT - SPACE_SHIP_WIDTH: # MOVE RED DOWN
        red.y += VEL

def handle_bullets(yellow_bullets, red_bullets, yellow, red):
    """ Controls bullet movement and collisions
    :return None
    """
    # Controls Yellow Bullet Interactions
    for bullet in yellow_bullets:
        bullet.x += BULLET_VEL # Controls bullet movement and speed
        # Detects collisions of yellow bullets with red ship
        if red.colliderect(bullet):
            pygame.event.post(pygame.event.Event(RED_HIT))
            yellow_bullets.remove(bullet)
        # Removes bullet once off screen
        elif bullet.x > WIDTH:
            yellow_bullets.remove(bullet)

    # Controls Red Bullet Interactions
    for bullet in red_bullets: 
        bullet.x -= BULLET_VEL # Controls bullet movement and speed
        # Detects collisions of red bullets with yellow ship
        if yellow.colliderect(bullet):
            pygame.event.post(pygame.event.Event(YELLOW_HIT))
            red_bullets.remove(bullet)
        # Removes bullet once off screen
        elif bullet.x < 0:
            red_bullets.remove(bullet)

def draw_winner(text):
    """ Draws text on screen once a player wins
    :param text: words to display on screen
    :return None
    """
    draw_text = WINNER_FONT.render(text, 1, WHITE) # preparing text
    WIN.blit(draw_text, (WIDTH//2 - draw_text.get_width()//2, HEIGHT//2 - draw_text.get_height()//2)) # placing on screen
    pygame.display.update() # updating the screen
    pygame.time.delay(5000) # pauses game to show winner before restart

def main():
    """
    :return None
    """
    # Initial placement/size of spaceships
    red = pygame.Rect(700, 300, SPACE_SHIP_WIDTH, SPACE_SHIP_HEIGHT)
    yellow = pygame.Rect(100, 300, SPACE_SHIP_WIDTH, SPACE_SHIP_HEIGHT)

    # Health value each player starts with
    red_health = 10
    yellow_health = 10

    # Lists that hold the bullets
    red_bullets = []
    yellow_bullets = []

    clock = pygame.time.Clock() #Initialize the Clock class of Pygame to control game speed
    run = True # While loop control variable
    while run:
        clock.tick(FPS) # Max speed of game set by FPS variable
        # Looking for events raised by PyGame during play
        for event in pygame.event.get():
            # Exit condition
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

            # Detects keypresses for bullet firing
            if event.type == pygame.KEYDOWN:
                # Controls yellow bullets being fired
                if event.key == pygame.K_LALT and len(yellow_bullets) < MAX_BULLETS:
                    bullet = pygame.Rect(yellow.x + yellow.width, yellow.y + yellow.height//2 -2, 10, 5)
                    yellow_bullets.append(bullet)
                    BULLET_FIRE_SOUND.play()

                # Controls red bullets being fired
                if event.key == pygame.K_RALT and len(red_bullets) < MAX_BULLETS:
                    bullet = pygame.Rect(red.x, red.y + red.height//2 -2, 10, 5)
                    red_bullets.append(bullet)
                    BULLET_FIRE_SOUND.play()

            # Reacts to red being hit
            if event.type == RED_HIT:
                red_health -= 1
                BULLET_HIT_SOUND.play()

            # Reacts to yellow being hit
            if event.type == YELLOW_HIT:
                yellow_health -= 1
                BULLET_HIT_SOUND.play()

        # Display winning message to player when health zero or less
        winner_text = ""
        # Yellow Wins
        if red_health <= 0:
            winner_text = "Yellow Wins!"
        # Red Wins
        if yellow_health <= 0:
            winner_text = "Red Wins!"
        # Draw winner on screen if not blank
        if winner_text != "":
            draw_winner(winner_text)
            timestamp = datetime.datetime.now()
            with open("winnerLog.txt", "a") as f:
                f.write(f"\n {timestamp} - {winner_text}")
            break
        
        # Looking for Key Presses
        keys_pressed = pygame.key.get_pressed() # Pygame detecting pressed keys
        yellow_handle_movement(keys_pressed, yellow) # Check if keys controlling yellow are pressed
        red_handle_movement(keys_pressed, red) # Check if keys controlling red are pressed

        # Handling Bullets
        handle_bullets(yellow_bullets, red_bullets, yellow, red)

        # Drawing window
        draw_window(red, yellow, red_bullets, yellow_bullets, red_health, yellow_health)

    # Recursive call of main() function to allow continous game-play
    main()

######################
#### MAIN PROGRAM ####
######################
# Will only work if this file contains main() function
if __name__ == "__main__":
    # Program call
    main()