#!/usr/bin/python3

# Author: Brad Penney
# Purpose: Event Fee Generator
# Date: 2022-09-26

# Inital variables to set program state
moreGuests = True
guestList = []

# Initial messages to the end-user
print("#### Welcome to the Event Fee Generator! ####")
print("Enter guest ages below. When there are no more guests, enter age zero to end.")
print("Prices are rounded to the nearest dollar and include taxes.\n")

# While loop finds guest ages, appends appropriate value to guestList
while moreGuests:
    nextGuest = input("What is the age of the next guest? (0 to quit): ")
    nextGuest = int(nextGuest)
    if nextGuest == 0:
        moreGuests = False
    elif nextGuest <= 5:
        print("\tThis guest is a Child, which gets in for free.\n")
    elif nextGuest <= 18:
        print("\tThis guest is a Youth, which costs $15.\n")
        guestList.append(15)
    elif nextGuest >= 60:
        print("\tThis guest is a Senior, which costs $10.\n")
        guestList.append(10)
    else:
        print("\tThis guest is an Adult, which costs $25.\n")
        guestList.append(25)
    

# Calulate the sum of the guestList and add taxes
totalCost = round(sum(guestList) * 1.15)

# Final message to end-user
print("\nThe total cost for your party, including taxes, is $" + str(totalCost) + " (rounded to the nearest dollar).\n")
